//
//  ViewController.swift
//  monteroH
//
//  Created by Hugo Montero on 5/6/18.
//  Copyright © 2018 Hugo Montero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBAction func nextButtonPreseed(_ sender: Any) {
        
        let urlString = "https://api.myjson.com/bins/72936"
        let session = URLSession.shared
        let url = URL(string: urlString)
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else {
                print("Error no data")
                return
            }
            guard let jsonInfo = try? JSONDecoder().decode(examenInfo.self, from: data) else {
                print("Error decoding Json")
                return
            }
            
            DispatchQueue.main.async {
                self.viewTitleLabel.text = "\(jsonInfo.json[0].viewTitle)"
                self.dateLabel.text = "\(jsonInfo.json[1].date)"
                
            }
            print(jsonInfo.json[1].viewTitle)
        }
             task.resume()
    }
}

